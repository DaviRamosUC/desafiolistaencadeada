package application;

public class ListaEncadeada {

	private Celula primeiro;
	private Celula ultimo;
	private Celula ponteiro;
	
	
	public void add(Aluno aluno) {
		Celula celula = new Celula();
		celula.setValor(aluno);
		
		if (primeiro == null && ultimo == null) {
			primeiro = celula;
			ultimo = celula;
		}else {
			ultimo.setProximo(celula);
			ultimo = celula;			
		}
	}
	
	public boolean temProximo() {
		if (primeiro == null) {
			return false;
		}else if(ponteiro == null) {
			ponteiro = primeiro;
			return true;
		}else {
			boolean temProximo = ponteiro.getProximo() != null ? true : false;
			ponteiro = ponteiro.getProximo();
			return temProximo;
		}
		
	}

	public Celula getPonteiro() {
		return ponteiro;
	}

	public void setPonteiro(Celula ponteiro) {
		this.ponteiro = ponteiro;
	}	
	
}
