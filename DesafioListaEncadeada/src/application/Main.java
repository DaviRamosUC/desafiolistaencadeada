package application;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		ListaEncadeada listaEncadeada = new ListaEncadeada();
		addAluno(listaEncadeada);
		
		System.out.println("A lista tem os seguintes alunos: ");
		while (listaEncadeada.temProximo()) {
			System.out.println(listaEncadeada.getPonteiro().getValor().toString());
		}
	}

	public static void addAluno(ListaEncadeada listaEncadeada) {
		Scanner sc = new Scanner(System.in);
		
		boolean op;

		do {
			System.out.print("Informe o nome do aluno: ");
			String nomeAluno = sc.nextLine();
			System.out.print("Informe a matricula do aluno: ");
			String matriculaAluno = sc.nextLine();
			
			listaEncadeada.add(new Aluno(nomeAluno, matriculaAluno));
			
			System.out.print("Deseja cadastrar um outro aluno? s/n: ");
			char resp = sc.nextLine().charAt(0);
			op = (resp == 's') ? true : false;
		} while (op);

		sc.close();
	}

}
